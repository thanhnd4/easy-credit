$(document).ready(function(){
if (window.FileList && window.File && window.FileReader) {

    $('.input_file').change(function(){   
        var input_file_this = $(this); 
        if (this.files && this.files[0]) {
            file_data = this.files[0];
            var fileReader = new FileReader();
            fileReader.onload = function(e) {
            var srcData = e.target.result;
            input_file_this.parents('.box_imgcmt').find('.img_show').attr('src',srcData);  
            };
            fileReader.readAsDataURL(file_data);
            input_file_this.parents('.box_imgcmt').find('.label_file_cmt').hide();
            input_file_this.parents('.box_imgcmt').find('.box_img_show').show();
        }
    })

    $('.input_file_muti').change(function(){
        var input_file_this = $(this); 
        var length  = this.files.length;
        var i;
        for( i = 0; i< length ; i++){
            if (this.files && this.files[i]) {
                file_data = this.files[i];
                var fileReader = new FileReader();
                fileReader.onload = function(e) {
                    var srcData = e.target.result;
                    data = '<div class="img_input_muti position_r">'+
                                    '<span class="del_img position_a" onclick="del_img(this)">'+
                                        '<i class="fas fa-times text-danger"></i>'+
                                    '</span>'+
                                    '<img src="'+srcData+'" alt="">'+
                                '</div>';
                    input_file_this.parents('.prarent_muti').find('.box_item_img').prepend(data);
                };
                fileReader.readAsDataURL(file_data);
                
            }
        }
    })

}

$('.card_item .card-header').click(function(){
    $(this).parents('.card_item').find('.card-body').slideToggle(300);
})

function  del_img(element){
    $(element).parents('.img_input_muti').remove();
}

function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

$('.number_vnd').keyup(function(){
    var value = $(this).val();
    value  = value.replace(/[^0-9,]/g, '');
    var value_place = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    $(this).val(value_place);
})

// oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
$(".number_vnd").on('input',function(){
    this.value = this.value.replace(/[^0-9.]/g, '');
    this.value = this.value.replace(/(\..*)\./g, '$1');
});
$('.rdb_sohuubds').change(function(){
    value_bds = $(this).val();
    if(value_bds == 1 ){
        $('.sohuubds').show(300);
    }
    else{
        $('.sohuubds').hide(300);
    }
})

$('.select_htgn').change(function(){
    value_htgn = $(this).val();
    if(value_htgn == 1 ){
        $('.div_htgn').show(300);
    }
    else{
        $('.div_htgn').hide(300);
    }
})
$(".btn_trang").click(function(){
    history.back();
});

$('#check_trung_adress').change(function(){
    if(this.checked == true){
        var sel_nation1 = $('.sel_nation1').val();
        var sel_city1 = $('.sel_city1').val();
        var sel_district1 = $('.sel_district1').val();
        var sel_wards1 = $('.sel_wards1').val();
        var sonhango1 = $('.sonhango1').val();
        if(sel_nation1 != '' && sel_city1 != '' && sel_district1 != '' && sel_wards1 != '' && sonhango1 != ''){
            $('.sel_nation2').val(sel_nation1);
            $('.sel_city2').val(sel_city1);
            $('.sel_district2').val(sel_district1);
            $('.sel_wards2').val(sel_wards1);
            $('.sonhango2').val(sonhango1);
        }
    }
})

});










